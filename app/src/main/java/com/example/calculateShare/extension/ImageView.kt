package com.example.calculateShare.extension

import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy.NONE

fun AppCompatImageView.loadRes(@DrawableRes drawableRes: Int) {
    Glide.with(this)
        .load(drawableRes)
        .diskCacheStrategy(NONE)
        .into(this)
}

fun AppCompatImageView.loadCircle(any: Any) {
    Glide.with(this).load(any)
        .circleCrop()
        .diskCacheStrategy(NONE)
        .into(this)
}

fun AppCompatImageView.loadUrl(
    url: String,
    @DrawableRes placeholderRes: Int? = null,
    @DrawableRes onErrorRes: Int? = null
) {
    val builder = Glide
        .with(this)
        .load(url)
        .diskCacheStrategy(NONE)

    placeholderRes?.run {
        builder.placeholder(placeholderRes)
    }
    onErrorRes?.run {
        builder.error(this)
    }

    builder.into(this)
}