package com.example.calculateShare.extension

import android.content.Context
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import timber.log.Timber
import java.net.URI
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

fun OkHttpClient.Builder.addSslSpinner(
    url: String,
    pinRoot: String?,
    pinIntermediate: String?,
    pinLeaf: String?
): OkHttpClient.Builder {
    if (pinRoot.isNullOrEmpty() || pinIntermediate.isNullOrEmpty() || pinLeaf.isNullOrEmpty()) {
        return this
    }

    val uri = URI.create(url)
    val host = uri.host
    val certificatePinner = CertificatePinner.Builder()
        .add(host, pinRoot)
        .add(host, pinIntermediate)
        .add(host, pinLeaf)
        .build()
    this.certificatePinner(certificatePinner)
    return this
}

fun OkHttpClient.Builder.addSslSocketFactory(
    context: Context,
    pinRoot: String?,
    pinIntermediate: String?,
    pinLeaf: String?
): OkHttpClient.Builder {

    if (pinRoot.isNullOrEmpty() || pinIntermediate.isNullOrEmpty() || pinLeaf.isNullOrEmpty()) {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun checkClientTrusted(
                    chain: Array<out X509Certificate>?,
                    authType: String?
                ) {
                }

                override fun checkServerTrusted(
                    chain: Array<out X509Certificate>?,
                    authType: String?
                ) {
                }

                override fun getAcceptedIssuers() = arrayOf<X509Certificate>()
            })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory
            sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            hostnameVerifier { _, _ -> true }.build()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }
    return this
}