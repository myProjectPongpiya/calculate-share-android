package com.example.calculateShare

import android.app.Application
import android.util.Log
import com.example.calculateShare.component.di.AppInjector
import com.example.calculateShare.component.helper.AppPrefs
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject

class OneStopApp : Application(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)
        initLog()
        AppPrefs.initEncryptedPrefs(this)
//        AndroidThreeTen.init(this)
    }

    private fun initLog() {
        if (BuildConfig.DEBUG) {
            Timber.plant(object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String? {
                    return (super.createStackElementTag(element) + " : "
                            + element.methodName + "\t"
                            + element.lineNumber)
                }
            })
        } else {
            Timber.plant(object : Timber.Tree() {
                private val CRASHLYTICS_KEY_PRIORITY = "priority"
                private val CRASHLYTICS_KEY_TAG = "tag"
                private val CRASHLYTICS_KEY_MESSAGE = "message"
                private var memberCode: String? = null
                override fun log(
                    priority: Int,
                    tag: String?,
                    message: String,
                    throwable: Throwable?
                ) {
                    if (priority == Log.ERROR || priority == Log.DEBUG) {
                        val crashlytics = FirebaseCrashlytics.getInstance()
                        crashlytics.log(message)
                        memberCode?.run {
                            crashlytics.setUserId(this)
                        }
                        tag?.run {
                            crashlytics.setCustomKey(CRASHLYTICS_KEY_TAG, tag)
                        }
                        throwable?.run {
                            crashlytics.recordException(throwable)
                        }
                    }
                }
            })
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector
}