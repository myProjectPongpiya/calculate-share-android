package com.example.calculateShare.component.di.module

import android.content.Context
import com.example.calculateShare.OneStopApp
import com.example.calculateShare.extension.addSslSocketFactory
import com.example.calculateShare.extension.addSslSpinner
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.time.Clock
import java.time.ZoneId
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideContext(oneStopApp: OneStopApp): Context =
        oneStopApp.applicationContext

    @Singleton
    @Provides
    fun provideClock(): Clock = Clock.system(ZoneId.systemDefault())

    @Singleton
    @Provides
    fun provideRetrofit(
        context: Context,
        gson: Gson
    ): Retrofit.Builder {
        val builder = OkHttpClient().newBuilder()
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)
        builder.writeTimeout(30, TimeUnit.SECONDS)
        builder.addSslSpinner(
            "",
            "",
            "",
            ""
        )
            .addSslSocketFactory(
                context,
                "",
                "",
                ""
            )
        return Retrofit.Builder()
            .client(builder.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            .create()
    }
}