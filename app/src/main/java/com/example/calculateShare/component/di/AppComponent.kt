package com.example.calculateShare.component.di

import com.example.calculateShare.OneStopApp
import com.example.calculateShare.component.di.module.AppModule
import com.example.calculateShare.component.di.module.MainActivityModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        MainActivityModule::class]
)

interface AppComponent : AndroidInjector<OneStopApp> {
    @Component.Factory
    interface Factory : AndroidInjector.Factory<OneStopApp>
}