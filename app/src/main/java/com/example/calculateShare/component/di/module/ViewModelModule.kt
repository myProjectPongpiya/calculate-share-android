package com.example.calculateShare.component.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.calculateShare.component.di.ViewModelKey
import com.example.calculateShare.component.viewmodel.ViewModelFactory
import com.example.calculateShare.ui.screens.FirstPageViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FirstPageViewModel::class)
    abstract fun bindFirstPageViewModel(viewModel: FirstPageViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
