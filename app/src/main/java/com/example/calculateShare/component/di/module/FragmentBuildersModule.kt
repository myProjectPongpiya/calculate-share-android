package com.example.calculateShare.component.di.module

import com.example.calculateShare.ui.screens.FirstPageFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeFirstPageFragment(): FirstPageFragment

}
