package com.example.calculateShare.component.helper

import android.content.Context
import android.content.SharedPreferences
import com.example.calculateShare.BuildConfig

object AppPrefs {
    lateinit var encryptedPrefs: SharedPreferences
    lateinit var encryptedStaticPrefs: SharedPreferences
    private const val CHECK_PREFS = "PREFS"

    fun initEncryptedPrefs(context: Context) {
        encryptedPrefs = context.getSharedPreferences(
            "${BuildConfig.APPLICATION_ID}.local_data",
            Context.MODE_PRIVATE
        )
        encryptedStaticPrefs = context.getSharedPreferences(
            "${BuildConfig.APPLICATION_ID}.local_data_s",
            Context.MODE_PRIVATE
        )
    }

    fun remove(key: String) {
        encryptedPrefs.edit().remove(key).apply()
    }

    fun clearAll() {
        encryptedPrefs.edit().clear().apply()
    }

    fun clearAllData() {
        encryptedStaticPrefs.edit().clear().apply()
        encryptedPrefs.edit().clear().apply()
    }
}