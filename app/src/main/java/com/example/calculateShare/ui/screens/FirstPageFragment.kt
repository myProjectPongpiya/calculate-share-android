package com.example.calculateShare.ui.screens

import com.example.calculateShare.R
import com.example.calculateShare.databinding.FragementFirstPageBinding
import com.example.calculateShare.ui.base.BaseFragment

class FirstPageFragment : BaseFragment<FragementFirstPageBinding, FirstPageViewModel>() {

    override val layoutId: Int = R.layout.fragement_first_page

    override val viewModelClass: Class<FirstPageViewModel>
        get() = FirstPageViewModel::class.java

    override fun onFragmentStart() {
        dataBinding.fragment = this
        dataBinding.vm = viewModel
    }

    private fun setUpView() {

    }
}
