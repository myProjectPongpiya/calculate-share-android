package com.example.calculateShare.ui.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.example.calculateShare.R
import com.example.calculateShare.component.di.Injectable
import com.example.calculateShare.component.lib.eventbus.AppEvent
import com.example.calculateShare.component.lib.eventbus.EventBus
import com.example.calculateShare.component.lib.eventbus.ToggleLoading
import com.example.calculateShare.component.lib.lifecycle.observeEvent
import com.example.calculateShare.component.uti.autoCleared
import com.example.calculateShare.ui.base.binding.FragmentDataBindingComponent
import com.google.android.material.appbar.MaterialToolbar
import timber.log.Timber
import javax.inject.Inject


abstract class BaseFragment<FragmentDataBinding : ViewDataBinding,
        FragmentViewModel : BaseViewModel>
    : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected lateinit var viewModel: FragmentViewModel
    protected lateinit var dataBindingComponent: DataBindingComponent

    protected var dataBinding by autoCleared<FragmentDataBinding>()
    abstract val layoutId: Int

    abstract val viewModelClass: Class<FragmentViewModel>

    protected var toolbar: MaterialToolbar? = null
    open fun onCreateView(rootView: View) {}

    abstract fun onFragmentStart()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.onAlertEvent.observeEvent(viewLifecycleOwner, this::onAlert)
        viewModel.onViewLoadingEvent.observeEvent(viewLifecycleOwner, this::onViewLoading)
        viewModel.onExitAppEvent.observeEvent(viewLifecycleOwner) {
            Timber.d("[EXIT]onExitAppEvent = %s", it)
            activity?.finish()
        }

        EventBus.listen(ToggleLoading::class.java)
            .subscribe {
                Timber.d("ToggleLoading :: %s", it)
                when (it) {
                    ToggleLoading.ShowViewLoading -> onViewLoading(true)
                    ToggleLoading.HideViewLoading -> onViewLoading(false)
                    else -> {
                    }
                }
            }.also {
                viewModel.addDisposableInternal(it)
            }

        onFragmentStart()
    }

    open fun showOkAlert(
        code: String? = "",
        title: String? = null,
        message: String,
        onDismiss: (() -> Unit)? = null
    ) {
        viewModel.onAlertEvent.setEventValue(
            Alert.OkDialog(
                code = code,
                title = title,
                message = message,
                onDismiss = onDismiss
            )
        )
    }

    open fun showSelectAlert(
        code: String? = null,
        title: String? = null,
        message: String,
        okButtonLabel: String? = "OK",
        cancelButtonLabel: String? = "Cancel",
        okButtonBg: Int = R.drawable.button_main,
        cancelButtonBg: Int = R.drawable.button_empty,
        onOkDismiss: (() -> Unit)? = null,
        onCancelDismiss: (() -> Unit)? = null
    ) {
        viewModel.onAlertEvent.setEventValue(
            Alert.SelectDialog(
                code = code,
                title = title,
                message = message,
                okButtonLabel = okButtonLabel,
                cancelButtonLabel = cancelButtonLabel,
                okButtonBg = okButtonBg,
                cancelButtonBg = cancelButtonBg,
                onOkDismiss = onOkDismiss,
                onCancelDismiss = onCancelDismiss
            )
        )
    }

    protected fun popBackStack() {
        Timber.d("popBackStack")
        findNavController().popBackStack()
    }

    protected fun popToRoot() {
        Timber.d("popToRoot")
        findNavController().navigateUp()
    }

    open fun onNavBackClick() {
        Timber.d("onNavBackClick")
        popBackStack()
    }

    @SuppressLint("ClickableViewAccessibility")
    open fun hideSoftKeyBoard() {
        view?.setOnTouchListener { v, event ->
            false
        }
    }

    protected fun onViewLoading(show: Boolean) {
//        if (show) {
//            dataBinding.root.findViewById<View>(R.id.loadingView)?.visible()
//        } else {
//            dataBinding.root.findViewById<View>(R.id.loadingView)?.gone()
//        }
    }

    protected fun onAlert(alert: Alert) {
        EventBus.publish(ToggleLoading.HideProgress)
        if (alert is Alert.Toast) {
            context?.run {
                AlertMessageDialog.showToast(this, alert)
            }
        } else {
            AlertMessageDialog.create(alert).show(childFragmentManager)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBindingComponent = FragmentDataBindingComponent(this)
        DataBindingUtil.setDefaultComponent(dataBindingComponent)
        dataBinding = DataBindingUtil.inflate(
            inflater,
            layoutId,
            container,
            false, dataBindingComponent
        )

        dataBinding.lifecycleOwner = this
        onCreateView(dataBinding.root)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(viewModelClass)
        dataBinding.lifecycleOwner = viewLifecycleOwner
        (viewModel as BaseViewModel).apply {
            gotoPage = {
                gotoPage(it)
            }
            popBackStack = {
                popBackStack()
            }
            popToRoot = {
                popToRoot()
            }
        }
//        toolbar = dataBinding.root.findViewById(R.id.toolbar)
//        toolbar?.setOnClickListener {
//            onNavBackClick()
//        }
    }


    private fun navController() = findNavController()
    protected fun gotoPage(page: NavDirections) {
        Timber.d(
            "currentDestination = %s, page=%s",
            navController().currentDestination?.id,
            page.actionId
        )
        try {
            if (navController().currentDestination?.id == null) {
                onAlert(
                    Alert.OkDialog(
                        message = getString(R.string.err_refresh_app), onDismiss = {
                            EventBus.publish(AppEvent.ReloadApp)
                        })
                )
            } else {
                navController().navigate(page)
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }
}